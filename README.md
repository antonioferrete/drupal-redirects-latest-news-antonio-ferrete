# Drupal Redirects & Latest news #

## General Instructions ##

For this exercise you will be starting a fresh Drupal 8 installation before doing the two exercises.

You should create a branch name it "drupalRedirectsLatestNews-Candidate-Name-DDMMYYY" and create a pull request to the master branch.

### Bootstrap Drupal 8 codebase and contrib modules ###

1. Bootstrap the repository to include the latest Drupal 8 version
2. Install the "Admin Toolbar" (admin_toolbar) contrib module in your codebase
3. Change the Drupal Icon in the Admin Toolbar module to the Isobar one (i.e. https://upload.wikimedia.org/wikipedia/commons/8/89/ISOBAR_LOGO_RGB_POS.jpg)

## Drupal Redirects Instruction ##

Develop a custom module to handle homepage redirect simple rule

#### Objective ####

Create the Homepage Redirect (homepage_redirect) custom module

1. Create a configuration form with:
	1. Checkbox to turn on redirect rule
	2. When the checkbox is on show a URL field for the destination URL
2. Show a menu entry for the Form in the Structure menu
3. Create one custom permission to manage the module (save and view the form)
4. Develop the code that whenever the user accesses homepage if the redirect rule is on, redirects the user to the destination URL with a 302 status code

## Latest news Instructions ##

### Objective ###

Create a new module called Latest news (latest_news) that should list the latest news in every page.

1. Create a block with the latest news showing only the title for Anonymous and for the logged in users, the title and the last modified date

### Assumptions ###

1. The usage of the views module is not allowed for creating this block
2. The website is meant to be run with all the caches enabled

## Export your Database settings ##

1. Export your Drupal configs for easily import in a local installation (installed modules, content types, menus etc)

Provide any instructions that might be needed to run your code.

